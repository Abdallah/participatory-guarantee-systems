### MOROCCO

#### 2019

#clingo --const target=2019 constraints/main.lp agroecologie-maroc/config.lp agroecologie-maroc/data.rabat.2018.lp agroecologie-maroc/data.rabat.2019.lp

#### 2020

#clingo --const target=2020 constraints/main.lp agroecologie-maroc/config.lp agroecologie-maroc/data.rabat.2018.lp agroecologie-maroc/data.rabat.2019.lp agroecologie-maroc/data.rabat.2020.lp

### FRANCE

#### 2019
#clingo --const target=2019 constraints/main.lp nature-progres/config.lp nature-progres/data.montpellier.2019.lp


#### 2020
#clingo --const target=2020 constraints/main.lp nature-progres/config.lp nature-progres/data.montpellier.2019.lp nature-progres/data.montpellier.2020.lp


### INDIA

##### 2019
#clingo --const target=2019 constraints/main.lp india-organic/config.lp india-organic/data.simulated-south-andaman-islands.2019.lp

##### 2020
#clingo --const target=2020 constraints/main.lp india-organic/config.lp india-organic/data.simulated-south-andaman-islands.2019.lp india-organic/data.simulated-south-andaman-islands.2020.lp

##### 2021
#clingo --const target=2021 constraints/main.lp india-organic/config.lp india-organic/data.simulated-south-andaman-islands.2019.lp india-organic/data.simulated-south-andaman-islands.2020.lp india-organic/data.simulated-south-andaman-islands.2021.lp

### To have a visual representation of the review graph, add the constraints/graphviz.lp file and pipe to Adam M. Smith's python tool (in extern/). For instance
#### clingo --const target=2020 constraints/main.lp constraints/graphviz.lp agroecologie-maroc/config.lp agroecologie-maroc/data.rabat.2018.lp agroecologie-maroc/data.rabat.2019.lp agroecologie-maroc/data.rabat.2020.lp | python extern/lonsdaleite.py -dcub
